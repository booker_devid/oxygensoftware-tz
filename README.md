# Start Go with DLL
Timing:  
- 2 hourse - Change KVM, install windows 10, install tools  [RESULT: OK!]
- 1 hourse - Change Cmake, Nmake, make and other programm.  [RESULT: FAIL]
- 1 hourse - Write code and read documentation. [RESULT: OK!]
- 2 hourse - Dancing with tambourine [RESULT: FAIL]
- 1 min - Restart system [RESULT: OK!]
- 1 min - Magic! All Work!! [RESULT: OK!] 
- ~15 min - Write README.md file [RESULT: OK!]
- 30 min - Read ease scripting on Windows [RESULT: OK!]
- 1 min - Create make.bat [RESULT: OK!]

```AS RESULT - 6:48, долгая адаптация к Windows, возможно, если бы перезагрузку Windows произвел сразу после установки всех прогармм и системных путей %PATH%, то бубен на танец бы не пригласил)) ``` 
___

# Manual build

Please, create .\bin directory if use use make.bat or manual build with default path from example!)

## Build DLL

Use ```g++``` for build **dll**  

```
g++.exe -c .\src\dllmain.cpp -o .\bin\exVerWin.o
g++.exe -o .\bin\exVerWin.dll -s -shared .\bin\exVerWin.o -Wl,--subsystem,windows
```
or
```
g++.exe -c .\src\dllmain.cpp -o .\bin\exVerWin.o
g++.exe -o .\bin\exVerWin.o -s -shared .\bin\exVerWin.dll
```

## Build GOlang

```
go.exe build -o .\bin\AppExecuteble.exe .\src\main.go
```

# Auto build

You can use ```make.bat``` for automatic build code.  
WARNING!! before use make.bat you have to make sure that in your system installed:  
    - MINGW g++.exe  
    - go.exe
