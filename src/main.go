package main

import (
	"fmt"
	"syscall"
	"unsafe"
)

func main() {
	dllWindows, err := syscall.LoadDLL("exVerWin.dll")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	extractWindowsVersion, err := dllWindows.FindProc("extractWindowsVersion")
	var verWinInInt [3]int32
	var verWinInByte [20]byte
	var byteString []byte
	memoryAdderssVWI := uintptr(unsafe.Pointer(&verWinInInt[0]))
	memoryAdderssVWR := uintptr(unsafe.Pointer(&verWinInByte[0]))
	res, _, err := extractWindowsVersion.Call(memoryAdderssVWI, memoryAdderssVWR)
	if err != nil && err.Error() != "The operation completed successfully." {
		fmt.Println("Program error - ", err.Error())
		return
	}
	if res != 0 {
		fmt.Println("Program error - ", res)
		return
	}
	byteString = verWinInByte[0:]
	fmt.Println("Array version - ", verWinInInt)
	fmt.Println("String version - ", string(byteString))
}
